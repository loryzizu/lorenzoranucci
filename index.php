<!DOCTYPE HTML>
<!--
	Strata by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Lorenzo Ranucci</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="icon" href="images/logo.png" type="image/x-icon">
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body id="top">
		<!--<img src="images/logo.png" style="float: right; margin: 10px;">-->
		<br style="clear: both"/>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="#" class="image avatar"><img src="images/io_bn.jpg" alt="" /></a>
					<h1>Mi piace firmarmi come <img src="images/logo.png" style="width: auto; height: 2em">,<br />
						"faccio cose con i <strong>computer</strong>", <br />
					mi nutro di <strong>cinema</strong> e <strong>musica</strong>,<br />
						da grande farò il <strong>calciatore</strong> </h1>
				</div>
			</header>

		<!-- Main -->
			<div id="main">

				<!-- One -->
					<section id="info">

						<header class="major">
							<h2>Un po' di me...</h2>
						</header>
						<p>
						Sono <strong>Lorenzo Ranucci</strong>, consulente informatico, sviluppatore software ed esperto di Linked Open Data.
						</p>
						<p>
							Nel 2014, anno della mia laurea triennale in Informatica, inizia la mia esperienza lavorativa e continua la mia carriera
							universitaria, sempre in quel di Perugia.
						</p>
						<p>Lavoro attualmente come consulente informatico per il progetto
							<a href="http://dati.umbria.it/" target="_blank"><strong>Linked Open Data</strong></a> della
							<strong>Regione Umbria</strong>. <br>
						Sviluppo come freelance varie web application in linguaggio <strong>PHP</strong>
							usando framework quali <strong>Symfony</strong>, <strong>Magento</strong> e <strong>Joomla!</strong>. <br>
						In veste di sviluppatore frontend e backend ho curato applicazioni web <strong>Java Enterprise</strong>  per l'azienda <a href="http://www.umbriadigitale.it/" target="_blank"><strong>UmbriaDigitale</strong></a>. <br>
							Collaboro con l'azienda <a href="http://www.keen.it/" target="_blank"><strong>Keen</strong></a> allo sviluppo dell'applicazione
							<a href="http://www.easyeschool.it/" target="_blank"><strong>Easyschool</strong></a> in ambiente desktop e <strong>Android</strong>.</a>
						</p>
						<p>
							<a href="#contatti"><strong>Contattami</strong></a> per una consulenza informatica o per condividere nuove idee
						</p>
					</section>

				<!-- Two -->
					<section id="Lavori">
						<h2>Lavori</h2>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<!--<a href="#" class="icon fa-github"><span class="label">Github</span></a>-->
								<a href="https://umbriaopenapi.regione.umbria.it" target="_blank" class="image fit thumb"><img src="images/fulls/umbriaopenapi.png" alt="" /></a>
								<h3><a href="https://umbriaopenapi.regione.umbria.it" target="_blank">UmbriaOpenAPI</a></h3>
								<p>Web application in <strong>PHP</strong> con <strong>Symfony</strong> e <strong>BOT Telegram</strong> che utilizza dati pubblici.<br>
									Si tratta di una demo su come sfruttare la potenza dei <strong>Linked Open Data</strong>. </p>
							</article>
							<article class="6u 12u$(xsmall) work-item">
								<!--<a href="#" class="icon fa-github"><span class="label">Github</span></a>-->
								<a href="http://caiperugia.it" target="_blank" class="image fit thumb"><img src="images/thumbs/caiperugia.png"  /></a>
								<h3><a href="http://caiperugia.it" target="_blank">CAI Perugia</a></h3>
								<p>Web application in <strong>PHP</strong> con <strong>Joomla!</strong>.<br>
									Permette di gestire vari aspetti dell'organizzazione di attività sportive e culturali della sezione di Perugia del <strong>Club Alpino Italiano</strong>. </p>
							</article>
						</div>
						<div class="row">
							<article class="6u 12u$(xsmall) work-item">
								<!--<a href="#" class="icon fa-github"><span class="label">Github</span></a>-->
								<a href="https://caprio1906.it" target="_blank" class="image fit thumb"><img src="images/thumbs/caprio.png"  /></a>
								<h3><a href="https://caprio1906.it" target="_blank">Caprio1906</a></h3>
								<p>Web application di <strong>E-commerce</strong> in <strong>PHP</strong> con <strong>Magento</strong>.<br>
								In costruzione!
								</p>
							</article>
							<article class="6u 12u$(xsmall) work-item">
								<!--<a href="#" class="icon fa-github"><span class="label">Github</span></a>-->
								<a href="http://diskanner.com" target="_blank" class="image fit thumb"><img src="images/thumbs/diskanner.png"  /></a>
								<h3><a href="http://diskanner.com" target="_blank">Diskanner</a></h3>
								<p>Web application per il monitoraggio di records sul sito <a href="http://discogs.com/" target="_blank"><strong>Discogs</strong></a>
								</p>
							</article>
						</div>
						<!--<ul class="actions">-->
							<!--<li><a href="#" class="button">Full Portfolio</a></li>-->
						<!--</ul>-->
					</section>

				<!-- Three -->
					<section id="contatti">
						<h2>Contattami</h2>
						<div class="row">
							<div class="8u 12u$(small)">
								<form method="post" action="#contatti">
									<?php
									if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["message"])){
										if(mail("loryzizu@gmail.com", "Email ammiratore ".$_POST["name"]."|".$_POST["email"], $_POST["message"])){
											?>
											<p class="confirm-message">Messaggio spedito</p><br>
											<?php
										}
									}
									?>
									<div class="row uniform 50%">
										<div class="6u 12u$(xsmall)"><input type="text" name="name" id="nome" placeholder="Nome" required/></div>
										<div class="6u$ 12u$(xsmall)"><input type="email" name="email" id="email" placeholder="Email" required/></div>
										<div class="12u$"><textarea name="message" id="messaggio" placeholder="Messaggio" rows="4" required></textarea></div>
									</div>
									<br>
									<ul class="actions">
										<li><input type="submit" value="Invia" /></li>
									</ul>
								</form>

							</div>
							<div class="4u$ 12u$(small)">
								<ul class="labeled-icons">
									<li>
										<h3 class="icon fa-home"><span class="label">Indirizzo</span></h3>
										Via Annibale Vecchi 70A<br />
										Perugia, PG 06123<br />
										Italia
									</li>
									<li>
										<h3 class="icon fa-mobile"><span class="label">Telefono</span></h3>
										+39 3494191878
									</li>
									<li>
										<h3 class="icon fa-envelope-o"><span class="label">Email</span></h3>
										<a href="#">ralf@lorenzoranucci.com</a>
									</li>
								</ul>
							</div>
						</div>
					</section>



			</div>

		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<ul class="icons">
						<li><a href="https://bitbucket.org/loryzizu/" class="icon fa-bitbucket" target="_blank"><span class="label">Bitbucket</span></a></li>
						<li><a href="https://github.com/loryzizu" class="icon fa-github" target="_blank"><span class="label">Github</span></a></li>
						<li><a href="https://www.linkedin.com/in/lorenzo-franco-ranucci-859416b2/" class="icon fa-linkedin" target="_blank"><span class="label">LinkedIn</span></a></li>
						<li><a href="https://www.facebook.com/lorenzo.lorenzo91" class="icon fa-facebook" target="_blank"><span class="label">Facebook</span></a></li>
						<li><a href="mailto:ralf@lorenzoranucci.com" class="icon fa-envelope-o" target="_blank"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>Grazie per il design a: <a href="http://html5up.net" target="_blank">HTML5 UP</a></li>
						<li>Grazie per il logo a: <a href="https://logomakr.com" target="_blank">LogoMakr.com</a></li></li>
					</ul>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.poptrox.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-96187488-1', 'auto');
			ga('send', 'pageview');

		</script>

	</body>
</html>
